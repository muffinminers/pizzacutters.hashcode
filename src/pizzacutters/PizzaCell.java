package pizzacutters;

public class PizzaCell {
	
	public String type;
	public int row;
	public int col;
	
	public boolean used;

	public PizzaCell(String t, int row, int col) {
		this.type = t;
		this.row = row;
		this.col = col;
	}
}
