package pizzacutters;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Pizzacutter {

	public static int rows;
	public static int columns;
	public static int minIngr;
	public static int maxCells;
	
	public static int minCells; //calculated
	public static int tomatoes; //calculated
	public static int mushrooms; //calculated
	
	static String minorIngredient;
		
	public static void main(String[] args) {
		System.out.println("I'm the cutterling");
		
		String[] files = new String[]{"example", "small", "medium", "big"};
		int fileIndex = 0;
		String file = files[fileIndex];
		try (FileReader fr = new FileReader(file + ".in");
			 BufferedReader br = new BufferedReader(fr);
			 FileWriter fw = new FileWriter(file + ".out");
			 BufferedWriter bw = new BufferedWriter(fw);
			 PrintWriter pw = new PrintWriter(bw);) 
		{
			
			String[] bits = br.readLine().split(" ");
			rows = Integer.parseInt(bits[0]);
			columns = Integer.parseInt(bits[1]);
			minIngr = Integer.parseInt(bits[2]);
			maxCells = Integer.parseInt(bits[3]);
			
			System.out.println(rows + "\t rows");
			System.out.println(columns + "\t columns");
			System.out.println(minIngr + "\t minimum ingredients per slice");
			System.out.println(maxCells + "\t maximum cells per slice");
			
			minCells = minIngr * 2;
			System.out.println(minCells + "\t minimum cells per slice");
			
			String line;
			PizzaCell[][] pizza = new PizzaCell[rows][columns];
			int rowIdx = 0;
			while ((line = br.readLine()) != null) {
//				System.out.println(line);
				String[] cells = line.split("");
				for (int i = 0; i < columns; i++) {
					PizzaCell pc = new PizzaCell(cells[i], rowIdx, i);
					if ("T".equalsIgnoreCase(pc.type))
						tomatoes++;
					else 
						mushrooms++;
					pizza[rowIdx][i] = pc; 
				}
				rowIdx++;
			}

			System.out.println(tomatoes + "\t tomatoes/pomodori/Tomaten/paradeis");
			System.out.println(mushrooms + "\t mushrooms/funghi/Pilze/schwammln");
			
			minorIngredient = tomatoes < mushrooms ? "T" : "M";
			System.out.println("We have less " + minorIngredient);
			
			Offset[] offsets = Functions.calcAllPossibleOffsets(minIngr, maxCells);
			
			ArrayList<PizzaSlice> slices = new ArrayList<>();
			PizzaCell nextMI;
			while ((nextMI = Functions.getNextIngredient(minorIngredient, pizza, slices)) != null) {
				Functions2.calcSmallestSlice(nextMI, pizza, slices, offsets);
			}
			
			Functions3.writeResult(pw, slices);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
