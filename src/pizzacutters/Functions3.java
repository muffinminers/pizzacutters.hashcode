package pizzacutters;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

public class Functions3 {
	
	public static boolean checkCellUsed(PizzaCell pc, ArrayList<PizzaSlice> slices) {
		int r = pc.row;
		int c = pc.col;
		boolean used = false;
		for (int i = 0, cnt = size(slices); i < cnt && !used; i++) {
			PizzaSlice s = slices.get(i);
			if (s.first.row <= r && s.last.row >= r
				&& s.first.col <= c && s.last.col >= c)
				used = true;
		}
		return used;
	}

	public static void writeResult(PrintWriter pw, ArrayList<PizzaSlice> slices) {
		int cnt = size(slices);
		System.out.println("We've cut " + cnt + " slices");
		pw.println(cnt);
		for (int i = 0; i < cnt; i++) {
			PizzaSlice s = slices.get(0);
			pw.println(s.first.row + " " + s.first.col + " " + s.last.row + " " + s.last.col);
		}
		System.out.println("Outputfile has been written.");
	}
	
	public static int size(Collection<?> list) {
		return list != null ? list.size() : 0;
	}

	public static boolean checkNoOverlap(PizzaSlice ps, ArrayList<PizzaSlice> slices, PizzaCell[][] pizza) {
		boolean overlap = false;
		for (int i = 0, iCnt = ps.getCntRows(); i < iCnt && !overlap; i++) {
			for (int j = 0, jCnt = ps.getCntCols(); j < jCnt && !overlap; j++) {
				overlap = checkCellUsed(pizza[i][j], slices);
			}
		}
		return overlap;
	}
}
