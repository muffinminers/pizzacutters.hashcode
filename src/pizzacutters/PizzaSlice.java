package pizzacutters;

public class PizzaSlice {
	public PizzaCell first;
	public PizzaCell last;
	
	public PizzaSlice(PizzaCell f, PizzaCell l) {
		first = f;
		last = l;
	}

	public int getSize() {
		int height = last.row - first.row + 1;
		int width = last.col - first.col + 1;
		return height * width;
	}
	
	public int getCntRows() {
		return last.row - first.row + 1;
	}
	
	public int getCntCols() {
		return last.col - first.col + 1;
	}
}
