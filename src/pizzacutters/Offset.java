package pizzacutters;

public class Offset {
	public int rowOffset;
	public int colOffset;
	
	public Offset(int r, int c) {
		rowOffset = r;
		colOffset = c;
	}
}
