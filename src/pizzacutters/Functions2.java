package pizzacutters;

import java.util.ArrayList;

public class Functions2 {

	public static void calcSmallestSlice(PizzaCell nextMI, PizzaCell[][] pizza, ArrayList<PizzaSlice> slices, Offset[] offsets) {
		int lastRow = nextMI.row;
		int lastCol = nextMI.col;
		
		PizzaSlice ps;
		for (int i = 0, cnt = offsets.length; i < cnt; i++) {
			Offset os = offsets[i];
			int firstRow = lastRow - (os.rowOffset - 1);
			int firstCol = lastCol - (os.colOffset - 1);
			if (firstRow >= 0 && firstCol >= 0) {
				ps = new PizzaSlice(pizza[firstRow][lastRow], pizza[lastRow][lastCol]);
				if (Functions.checkSliceValid(Pizzacutter.minIngr, Pizzacutter.maxCells, ps, pizza)
					&& Functions3.checkNoOverlap(ps, slices, pizza));
					slices.add(ps);			
			}
		}
	}

}
