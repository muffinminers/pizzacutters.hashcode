package pizzacutters;

import java.util.ArrayList;

public class Functions {

	public static PizzaCell getNextIngredient(String ingr, PizzaCell[][] pizza, ArrayList<PizzaSlice> slices) {
		// TODO Auto-generated method stub
        //starting cell
        int row = 0;
        int col = 0;
        if(slices.size() > 0) {
            PizzaSlice startingCell = slices.get(slices.size() - 1);
            row = startingCell.first.row + 1;
            col = startingCell.last.col + 1;
        }

        int maxRows = pizza.length;
        int maxCols = pizza[0].length;
        int j = col;
        for(int i = row; i < maxRows - 1  && j < maxCols - 1; i += 2){
            if(pizza[i][j].type.equals(ingr)){
                return pizza[i][j];
            }
            else if(pizza[i+1][j].type.equals(ingr)){
                return pizza[i+1][j];
            }
            else if(pizza[i][j+1].type.equals(ingr)){
                return pizza[i][j+1];
            }
            else if(pizza[i+1][j+1].type.equals(ingr)){
                return pizza[i+1][j+1];
            }
            j+=2;
        }
        return null;
	}

	public static boolean checkSliceValid(int minIngr, int maxSize, PizzaSlice slice, PizzaCell[][] pizza){
        int base = slice.last.row - slice.first.row;
        int height = slice.last.col - slice.first.col;
        int size = base * height;
        boolean rightSize;
        if(size < maxSize)
            rightSize = true;
        else
    	    return false;
        //check the ingredients
        int numMush = 0;
        int numTom = 0;
        for (int i = slice.first.row; i < slice.last.row; i++){
            for (int j = slice.first.col; j < slice.last.col; j++){
                if(pizza[i][j].type.equals("M"))
                    numMush++;
                else if(pizza[i][j].type.equals("T"))
                    numTom++;
            }
        }
        return numMush >= minIngr && numTom >= minIngr && rightSize;
    }

	public static Offset[] calcAllPossibleOffsets(int minIngr, int maxCells) {
        ArrayList<Offset> offsets = new ArrayList<>();
        int area = minIngr;
        while(area <= maxCells){
            AddOffsets(offsets, area);
            area+=1;
        }
        System.out.println("slices" + offsets.toString());
        Offset[] staticOffsetsArray = new Offset[offsets.size()];
        offsets.toArray(staticOffsetsArray);
		return staticOffsetsArray;
	}

    private static void AddOffsets(ArrayList<Offset> offsets, int area) {
        int divisor = 1;
        while (divisor < area / divisor) {
            if (area % divisor == 0) {
                int row = area / divisor;
                int col = divisor;
                Offset newOffset = new Offset(row, col);
                //if(!offsets.contains(newOffset))
                offsets.add(new Offset(row, col));
                if (row != col) {
                    offsets.add(new Offset(col, row));
                }
                divisor += 1;
            }
        }
    }

}
